*Last update: 2019-02-20 12:00*

EnMAP-Box Workshop 2019
=======================



.. toctree::
    :maxdepth: 2
    :caption: General

    general/workshop.rst

.. toctree::
    :maxdepth: 2
    :caption: Program

    day1/program.rst
    day2/program.rst
    day3/program.rst

.. .. toctree::
    :maxdepth: 2
    :caption: Application Tutorials

..    day1/application_TT1.rst

.. .. toctree::
    :maxdepth: 3
    :caption: Programming Tutorials

..    programming_tutorial2/setup_pycharmproject.rst
    programming_tutorial1/pt1.rst
    programming_tutorial2/00_tutorial_content.rst

